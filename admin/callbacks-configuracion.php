<?php
    require_once($_SERVER['DOCUMENT_ROOT']."/wp-config.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/wp-content/plugins/sync-inventario/admin/class/class-sync.php");

class CallbacksConfiguracion
{
	public function __construct() {
		$this->RegistraData();
	}

	private function RegistraData(){
		if(isset($_POST['submit']))
		{
			$objClaseSync = new ClaseSync();

			$Clave=$objClaseSync->GeneraLlave($_POST['syncClave'].$_POST['syncDominio']);
			$objClaseSync->Consulta("*"," sync_id = 1 ");
			$objClaseSync->sync_dominio = $_POST['syncDominio'];
			$objClaseSync->sync_clave = $_POST['syncClave'];
			$objClaseSync->sync_clave_encriptada = $Clave;//$_POST['syncPublica'];
			$objClaseSync->Guardar();

		}
	}

	public function __destruct()
	{
		$goback = add_query_arg( 'settings-updated', 'true', wp_get_referer() );
		wp_redirect( $goback );
	}
}

new CallbacksConfiguracion;

?>

