<?php
    require_once($_SERVER['DOCUMENT_ROOT']."/wp-config.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/wp-content/plugins/sync-inventario/admin/class/class-sync.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/wp-content/plugins/sync-inventario/admin/class/class-ordenes.php");


class CallbacksOrdenes
{
	public function __construct() {

		$this->RegistraData();
	}

	private function RegistraData(){

		if(isset($_POST['submit']))
		{
			$Ordenes = new stdClass();

			foreach( $_POST as $Variable => $Valor )
			{
				if( strpos($Variable,'chk') !== false)
				{
					list($Tabla,$Campo) = explode(":",$Valor);

					if( !isset($Ordenes->$Tabla) || !is_object($Ordenes->$Tabla) )
					{
						$Ordenes->$Tabla = new stdClass();
					}
					$Ordenes->$Tabla->$Campo = $_POST[$Campo];
				}
			}

			$objClaseSync = new ClaseSync();
			$objClaseSync->Consulta("*"," sync_id = 1 ");
 			$objClaseSync->sync_ordenes = json_encode($Ordenes);
 			$objClaseSync->Guardar();
		}
	}

	public function __destruct()
	{
		$goback = add_query_arg( 'settings-updated', 'true', wp_get_referer() );
		wp_redirect( $goback );
	}
}

new CallbacksOrdenes;

?>
