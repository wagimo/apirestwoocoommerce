<?php
    require_once($_SERVER['DOCUMENT_ROOT']."/wp-config.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/wp-content/plugins/sync-inventario/admin/class/class-sync.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/wp-content/plugins/sync-inventario/admin/class/class-productos.php");


class CallbacksProductos
{
	public function __construct() {

// 		$ObjProductos = new ClaseProductos();
// 
// 		$oProducto = json_decode(' 
// {"0":{
//     "nombre":"Zapatillas de Ruta xx12",
//     "marca":"GW",
//     "descripcion":"Zapatilla de ruta GW en carbono xxx",   
//     "precio":"458000",
//     "cantidad":25,
//     "codigo": 1112233222
//    
// },
// "1":{
//     "nombre":"Zapatillas de Ruta xx12",
//     "marca":"GW",
//     "descripcion":"Zapatilla de ruta GW en carbono xxx",   
//     "precio":"458000",
//     "cantidad":25,
//     "codigo": 1112233222
//    
// }
// }
// ');
//  		var_dump($oProducto);
// 		exit(0);

//		 $ObjProductos->sku = "1112233222";
//		$a = $ObjProductos->ProcesarEdicionProducto($oProducto);
		
		//$ObjProductos->ProcesarNuevoProducto($oProducto);
// 		exit(0);
 //ProcesarNuevoProducto

// 		//Consultar un producto
// 		$ObjProductos->sku = "45654544";
// 		$ObjProductos->ConsultaPorSKA();
// 
// 		//Ver el nombre del producto
// 		echo $ObjProductos->producto;
// 
// 		//COnulsta el valor de producto
// 		echo "<br />".$ObjProductos->precio."---";
// 		
// 		//Cambia el valor del producto
// 		$ObjProductos->precio = "12000";
// 
// 		//Actualizar un producto
// 		$ObjProductos->producto = "prueba 1";
// 		$ObjProductos->GuardarProducto();

		//Creo un nuevo producto
// 		$ObjProductos->id = "0";
// 		$ObjProductos->autor = "1";
// 		$ObjProductos->fecha = date("Y-m-d h:i:s");
// 		$ObjProductos->nombreMostrar = "producto de prueba a mostrar 2";
// 		$ObjProductos->producto = "producto de prueba a mostrar 2";
// 		$ObjProductos->tipo = "product";
//  		$ObjProductos->GuardarProducto();
// 
// 		//lsas propiedades del producto
//  		$ObjProductos->sku = "9988877";
//  		$ObjProductos->precio = "120000";

 		//exit(0);

		$this->RegistraData();
	}

	private function RegistraData(){

		if(isset($_POST['submit']))
		{
			$Producto = new stdClass();

			foreach( $_POST as $Variable => $Valor )
			{
				if( strpos($Variable,'chk') !== false)
				{
					list($Tabla,$Campo) = explode(":",$Valor);

					if( !isset($Producto->$Tabla) || !is_object($Producto->$Tabla) )
					{
						$Producto->$Tabla = new stdClass();
					}
					$Producto->$Tabla->$Campo = $_POST[$Campo];
				}
			}

			$objClaseSync = new ClaseSync();
			$objClaseSync->Consulta("*"," sync_id = 1 ");
 			$objClaseSync->sync_productos = json_encode($Producto);
 			$objClaseSync->Guardar();
		}
	}

	public function __destruct()
	{
		$goback = add_query_arg( 'settings-updated', 'true', wp_get_referer() );
		wp_redirect( $goback );
	}
}

new CallbacksProductos;

?>

