<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/wp-content/plugins/sync-inventario/includes/trait-db.php';

class ClaseOrdenes
{
	use SyncDB;

	public function __construct()
	{
		$this->initDB("posts");
	}

	public function GetConsultaTodos($param)
	{

		 $this->orden->Consulta("*"," post_status = " . $param);

		 if($this->posts == null){
				return null;
		 }

		 return $this->posts;
	}

	/**
	 * Obtiene un unico producto desde la Base de datos filtrado por el SKU enviado en la URL de la solicitud
	 */
	public function GetOrderTodos($param)
	{

		global $wpdb;
		$prefij = $wpdb->prefix;

		$sql = "SELECT
				ord.ID, ord.post_excerpt, GROUP_CONCAT(concat(ordmet.meta_key,':',ordmet.meta_value)) as datos,oitemprod.meta_value as idProd,
				oitem.order_item_name, oitemcant.meta_value as cantidad, oitemtotal.meta_value as valor, sku.meta_value as sku
						FROM " . $prefij . "posts ord
						join " . $prefij . "postmeta ordmet on ord.ID = ordmet.post_id
						join " . $prefij . "woocommerce_order_items oitem on ord.ID = oitem.order_id
						join " . $prefij . "woocommerce_order_itemmeta oitemcant on oitem.order_item_id = oitemcant.order_item_id and oitemcant.meta_key = '_qty'
						join " . $prefij . "woocommerce_order_itemmeta oitemtotal on oitem.order_item_id = oitemtotal.order_item_id and oitemtotal.meta_key = '_line_total'
						join " . $prefij . "woocommerce_order_itemmeta oitemprod on oitem.order_item_id = oitemprod.order_item_id and oitemprod.meta_key = '_product_id'
						left join " . $prefij . "postmeta sku on oitemprod.meta_value = sku.post_id and sku.meta_key = '_sku'
						where ord.post_status = 'wc-$param' and ord.post_type = 'shop_order'
									and ordmet.meta_key in ('_billing_first_name','_billing_last_name','_order_total','_pais','_departamento','_ciudad','_tipopersona','_cedulanit','_billing_company')
						and ord.ID NOT IN (SELECT logitem.sync_items_prodId  FROM wp_sync_log log
								join `wp_sync_log_items` logitem on log.sync_log_id = logitem.sync_log_id
								WHERE log.sync_log_metodo = 'orden_compra')
						GROUP by ord.ID, oitem.order_item_id";

		$objArray = $this->ConsultaJoin($sql);

		$objResultado = array();
		if($objArray == null){
			$objResultado['status'] = "error";
			$objResultado['mensaje'] = "No hay generadas ninguna orden para descargar.";
			return $objResultado;
		}

		$objSyncLog = new ClaseSyncLog();
		$objSyncLog->CreaLog("orden_compra");
		$OrdenLog = new ClaseSyncJson();
		$OrdenLog->init();

		$iCont = 0;
		$iContiTem = 0;
		$numId = 0;
		$objItem = null;

		foreach($objArray as $item){
			if($numId != $item->ID){
				if($numId != 0){
						$objResultado[$iCont]['items'] = $objItem;
						//Creo un objeto el cual llevara los datos a guardar en syn_log_items
						$OrdenLog->init();
						$OrdenLog->posts = new ClaseSyncDb("posts");
						$OrdenLog->posts->ID = $numId;
						$OrdenLog->Detalle = json_encode($objItem);

						//El objeto creado se envia a la funcion encargada de registrar en el log los campos afectados
						$objSyncLog->RegistraCambios($OrdenLog);
						
						$iCont++;
				}
				$iContiTem = 0;
				$objItem = null;
				$numId = $item->ID;
				$objDatos = explode(',',$item->datos);
				foreach($objDatos as $dato){
						list($key,$value) = explode(':',$dato);
						switch ($key) {
							case '_tipopersona':
								// code...
								$objResultado[$iCont]['Tipopersona'] = $value;
								break;
							case '_cedulanit':
								// code...
								$objResultado[$iCont]['documento'] = $value;
								break;
							case '_billing_first_name':
								// code...
								$objResultado[$iCont]['nombreCliente'] = $value;
								break;
							case '_billing_last_name':
								// code...
								$objResultado[$iCont]['nombreCliente'] .= ' ' . $value;
								break;
							case '_billing_company':
								if($value != '_')
									$objResultado[$iCont]['razonsocial'] .= ' ' . $value;
								break;
							case '_pais':
								// code...
								$objResultado[$iCont]['Pais'] = $value;
								break;
							case '_departamento':
								// code...
								$objResultado[$iCont]['Departamento'] = $value;
								break;
							case '_ciudad':
								// code...
								$objResultado[$iCont]['Ciudad'] = $value;
								break;
						}
				}

				$objResultado[$iCont]['observacion'] = $item->post_excerpt;
			}

			if($numId == $item->ID){
					// code...
					//// Si el sku e nulo se debe buscar el sku del log ////

					$objItem[$iContiTem]['sku'] = ($item->sku == null) ? $this->BuscarSku($item->idProd) : $item->sku;
					$objItem[$iContiTem]['producto'] = $item->order_item_name;
					$objItem[$iContiTem]['precioventa'] = $item->valor;
					$objItem[$iContiTem]['cantidad'] = $item->cantidad;


					$iContiTem++;
			}
		}

		$objResultado[$iCont]['item'] = $objItem;
		
		$OrdenLog->init();
		$OrdenLog->posts = new ClaseSyncDb("posts");
		$OrdenLog->posts->ID = $numId;
		$OrdenLog->Detalle = json_encode($objItem);

		//El objeto creado se envia a la funcion encargada de registrar en el log los campos afectados
		$objSyncLog->RegistraCambios($OrdenLog);

		return $objResultado;
	}
	
	private function BuscarSku($idProd){

		$sql = "SELECT logitem.sync_items_nuevo as sku FROM wp_sync_log_items logitem 
			WHERE logitem.sync_items_prodId = $idProd and logitem.sync_items_nombre = '_sku'
			order by logitem.sync_items_id desc";
			
		$objArray = $this->ConsultaJoin($sql);
		
		if(count($objArray) > 0){
			return $objArray[0]->sku;
		}
		else{
			return null;
		}
	}
}



?>
