<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/wp-content/plugins/sync-inventario/includes/trait-json.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/wp-content/plugins/sync-inventario/admin/class/class-sync-log.php';

class ClaseProductos
{
	use SyncJson;
	public $IdProducto = "0";
	

	public function __construct()
	{
		$this->initJSON("Productos");
	}

	/**
	 * Metodo principal que contiene la logica y reglas de negocio para realizar el ajuste del inventario
	 * teniendo en cuenta  el tipo de operación realizada en celeste para cada producto.
	 */
	public function AjustarCantidadesInventario($oProduct){
		
		$prepared_post  = new stdClass();

		$this->GetPostMetaBySKU();
		if($this->IdProducto == "0"){
			$prepared_post->status = "error";
			$prepared_post->mensaje = "El producto identificado con el SKU: ".$this->sku." no fue encontrado en la Base de Datos. Proceso de actualización de inventario cancelado.";
			return $prepared_post;			
		}		

		if($oProduct == null){
			$prepared_post->status = "error";
			$prepared_post->mensaje = "Error al leer el Json desde el body de la solicitud. Proceso de creación cancelado.";
			return $prepared_post;
		}

		if(!is_numeric($oProduct->cantidad))	{
			$prepared_post->status = "error";
			$prepared_post->mensaje = "El valor ingresado en las cantidades [" .$oProduct->cantidad."] no es del tipo numérico. Proceso cancelado!";
			return $prepared_post;
		}

		$stock = get_post_meta($this->IdProducto, '_stock', true );
		$nuevaCantidad = 0;
		switch($oProduct->operacion){

			case "venta":

				if($oProduct->cantidad > $stock){
					$prepared_post->status = "error";
					$prepared_post->mensaje = "Las unidades solicitadas para la venta [" .$oProduct->cantidad."] son mayores a las disponibles en el inventario [".$stock."]. Proceso cancelado!";
					return $prepared_post;
				}

				$nuevaCantidad = $stock - $oProduct->cantidad;
				break;

			case "compra":
			case "devolucion":
				
				if($oProduct->cantidad <= 0){
					$prepared_post->status = "error";
					$prepared_post->mensaje = "Las unidades a ingresar al inventario [" .$oProduct->cantidad."] no pueden ser menores o iguales a Cero. Proceso cancelado!";
					return $prepared_post;
				}
				$nuevaCantidad = $stock + $oProduct->cantidad;
				break;			
			case "notainventario":
				break;			
		}

		$AnteriorProducto[] = $this->GetPostById();
		$AnteriorProducto[] = $this->postmeta->Consulta(" GROUP_CONCAT('\"',meta_key ,'\":\"', meta_value,'\"' ) as metas"," post_id = ".$this->IdProducto);


		$objSyncLog = new ClaseSyncLog();
		$objSyncLog->CreaLog($oProduct->operacion);
		$objSyncLog->RegistraCambios($this,false,$AnteriorProducto);

		try
		{
			$this->UpdateCantidad($nuevaCantidad);
			$prepared_post->status = "ok";
			$prepared_post->mensaje = "Proceso realizado satisfactoriamente";
			return $prepared_post;
		}
		catch (Exception $e) {
			$prepared_post->status = "error";
			$prepared_post->mensaje = "Excepción capturada: ".$e->getMessage()."\n";
			return $prepared_post;
		}
	}
	/**
	 * Actualiza las unidades del inventario asociados al  producto comercializado y el estado del mismo.
	 * Si las unidades disponibles son iguales o menores a cero, se actualiza el estado del stock de Disponible  a Agotado.
	 * Se crea un registro en el log del PlugIn para saber la fecha, hora en que se afecto el inventario
	 */
	protected function UpdateCantidad($cantidad)
	{
		$status = $cantidad <= 0 ? 'outofstock' : 'instock';
		$stock = $cantidad < 0 ? 0 : $cantidad;

		update_post_meta( $this->IdProducto, '_stock',$stock);
		update_post_meta( $this->IdProducto, '_stock_status',$status );
	}

	/**
	 * Método principal que ejecuta la logica de creación de nuevos productos y metadatos.
	 */
	public function ProcesarNuevoProducto($oProduct){

		$prepared_post  = new stdClass();

		if($oProduct == null){
			$prepared_post->status = "error";
			$prepared_post->mensaje = "Error al leer el Json desde el body de la solicitud. Proceso de creación cancelado.";
			return $prepared_post;
		}
		
		$this->GetPostMetaBySKU($oProduct->codigo);

		if($this->IdProducto != "0" && $this->IdProducto != ""  ){
			$prepared_post->status = "error";
			$prepared_post->mensaje = "Ya existe un producto identificado con el SKU: ".$this->sku.". Proceso de creación del producto cancelado.";
			return $prepared_post;			
		}

		try {
			
			$resultPost = $this->GuardarProducto($oProduct);
			return $resultPost;				
		} 
		catch (Exception $e) {
			$prepared_post->status = "error";
			$prepared_post->mensaje = "Excepción capturada: ".$e->getMessage()."\n";
			return $prepared_post;
		}
	}


	public function ProcesarNuevoProductoBulk($oProducts){

		if($oProducts == null){
			$prepared_post->status = "error";
			$prepared_post->mensaje = "Error al leer el Json desde el body de la solicitud. Proceso de creación cancelado.";
			return $prepared_post;
		}

		try {
			
			$SqlMetaProductos = array();
			$iArr = 0;
			$iCont = 0;
			$SqlMetaProductos[$iArr]="INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES";

			foreach( $oProducts as $oProduct )
			{
				$this->posts->ID = "0";
				foreach($oProduct as $Campo => $Valor)
				{
					$this->$Campo = $Valor;
				}
	
				$this->posts->post_status = "draft";
				$this->posts->comment_status ="open";
				$this->posts->ping_status = "closed";
				$this->posts->post_type = "product";
				$this->posts->post_modified_gmt = date("Y-m-d h:i:s");
				$this->posts->post_modified = date("Y-m-d h:i:s");
				$this->posts->post_date_gmt = date("Y-m-d h:i:s");
				$this->posts->Guardar();
	
				if( $iCont == 500 )
				{
					$iArr++;
					$SqlMetaProductos[$iArr]="INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES";
					$iCont=0;
				}
				else
				{
					$iCont++;
				}

				$SqlMetaProductos[$iArr].="
							(null, ".$this->posts->ID.", '_sku', '".$oProduct->codigo."'),
							(null, ".$this->posts->ID.", '_regular_price', '".$this->postmeta->_price."'),
							(null, ".$this->posts->ID.", '_manage_stock', 'yes'),
							(null, ".$this->posts->ID.", '_virtual', 'yes'),
							(null, ".$this->posts->ID.", '_stock', '".$this->postmeta->_stock."'),
							(null, ".$this->posts->ID.", '_price', '".$this->postmeta->_price."'),";
			}

			global $wpdb;
			foreach( $SqlMetaProductos as $Sql)
			{
				$Sql = substr($Sql,0,strlen($Sql)-1);
				$wpdb->query($Sql);
			}

		} 
		catch (Exception $e) {
			$prepared_post->status = "error";
			$prepared_post->mensaje = "Excepción capturada: ".$e->getMessage()."\n";
			return $prepared_post;
		}
	}

	/**
	 * Coordina el llamado a los metodos de creacion del producto y metadatos validando el resultado devuelto
	 * por cada  uno de ellos.
	 */
	protected function GuardarProducto($oProduct)
	{		
		$prepared_post  = new stdClass();
		try{		

			$result = $this->NuevoProducto($oProduct);
			if($result != 'ok'){
				$prepared_post->status = "error";
				$prepared_post->mensaje = $result;
			}

// 			$resultmeta = $this->NuevoMetaData($oProduct);
// 			if($resultmeta != 'ok'){
// 				$prepared_post->status = "error";
// 				$prepared_post->mensaje = $resultmeta;
// 			}

//  			$objSyncLog = new ClaseSyncLog();
//  			$objSyncLog->CreaLog("post_product");
//  			$objSyncLog->RegistraCambios($this);

			$prepared_post->status = "ok";
			$prepared_post->mensaje = "Proceso realizado satisfactoriamente";
			return $prepared_post;
		}
		catch (Exception $e) {
			$prepared_post->status = "error";
			$prepared_post->mensaje = "Excepción capturada: ".$e->getMessage()."\n";
			return $prepared_post;
		}
		
	}

	/**
	 * Crea el producto en la tabla POST
	 */
	protected function NuevoProducto($oProduct){
		try{

			$this->posts->ID = "0";
			foreach($oProduct as $Campo => $Valor)
			{
				$this->$Campo = $Valor;
			}

			$this->posts->post_status = "draft";
			$this->posts->comment_status ="open";
			$this->posts->ping_status = "closed";
			$this->posts->post_type = "product";
			$this->posts->post_modified_gmt = date("Y-m-d h:i:s");
			$this->posts->post_modified = date("Y-m-d h:i:s");
			$this->posts->post_date_gmt = date("Y-m-d h:i:s");
			$this->posts->Guardar();

			return "ok";
		}
		catch (Exception $e) {
			$prepared_post->status = "error";
			$prepared_post->mensaje = "Excepción capturada: ".$e->getMessage()."\n";
			return $prepared_post;
		}
	}

	/**
	 * Crea los atributos dentro de la metadata para el nuevo producto 
	 */
	protected function NuevoMetaData($oProduct){
		try{

// 			add_post_meta($this->posts->ID,'_sku',$this->postmeta->_sku);
// 			add_post_meta($this->posts->ID,'_regular_price',$this->postmeta->_price <= 0 ? 0 : $this->postmeta->_price);
//  			add_post_meta($this->posts->ID,'_manage_stock',"yes");
//  			add_post_meta($this->posts->ID,'_virtual',"yes");
// 			add_post_meta($this->posts->ID,'_stock',$this->postmeta->_stock <= 0 ? 0 : $this->postmeta->_stock);
// 			add_post_meta($this->posts->ID,'_price',$this->postmeta->_price <= 0 ? 0 : $this->postmeta->_price);


/*			foreach($oProduct as $Campo => $Valor)
			{
				$this->$Campo = $Valor;
			}

 			update_post_meta( $this->IdProducto, '_regular_price', $this->postmeta->_price );*/
			$this->IdProducto = $this->posts->ID;
			return "ok";
		}
		catch (Exception $e) {
			$prepared_post->status = "error";
			$prepared_post->mensaje = "Excepción capturada: ".$e->getMessage()."\n";
			return $prepared_post;
		}
	}

	/**
	 * Método principal que ejecuta la logica de actualización del Producto y metadatos.
	 * Si el producto no existe en la base de datos, retorna un mensaje al controlador, que será devuelto
	 * a celeste como un error.
	 * @param $oProduct objeto con la información a editar	
	 * @param $call parametro que indica desde donde ha sido llamado el método, cuando es null es porque el metodo fue invicado
	 * 		  desde el controller, en caso contrario, fue invocado por la funcion Interna BulkUpdate con el fin de leer el SKU
	 * 		  directamente del objeto iterado y no del parámetro global.
	 */
	public function ProcesarEdicionProducto($oProduct, $call = null){		
		
		$prepared_post  = new stdClass();
		
		if($call == null){
			$this->GetPostMetaBySKU();
		}
		else{
			$this->GetPostMetaBySKU($oProduct->codigo);
		}
		
		if($this->IdProducto == "0" || $this->IdProducto == ""){
			$prepared_post->status = "error";
			$prepared_post->mensaje = "El producto identificado con el SKU: ".$this->sku." no fue encontrado en la Base de Datos. Proceso de actualización cancelado.";
			return $prepared_post;			
		}
		
		try {
			$AnteriorProducto[] = $this->GetPostById();
			$AnteriorProducto[] = $this->postmeta->Consulta(" GROUP_CONCAT('\"',meta_key ,'\":\"', meta_value,'\"' ) as metas"," post_id = ".$this->IdProducto);

			$result = $this->UpdatePostMeta($oProduct);
			if($result != "ok"){
				$prepared_post->status = "error";
				$prepared_post->mensaje = $result;
				return $prepared_post;
			}

			$resultPost = $this->UpdatePost($oProduct);
			if($resultPost != 'ok'){
				$prepared_post->status = "error";
				$prepared_post->mensaje = $resultPost;
				return $prepared_post;					
			}

 			$objSyncLog = new ClaseSyncLog();
 			$objSyncLog->CreaLog("update_product");
 			$objSyncLog->RegistraCambios($this,false,$AnteriorProducto);

			 $prepared_post->status = "ok";
			 $prepared_post->mensaje = "Proceso realizado satisfactoriamente!";
			 return $prepared_post;								
		} 
		catch (Exception $e) {
			$prepared_post->status = "error";
			$prepared_post->mensaje = "Excepción capturada: ".$e->getMessage()."\n";
			return $prepared_post;
		}

	}

	/**
	 * Actualiza el registro PostMeta con los valores provenientes de Celeste.
	 */
	protected function UpdatePostMeta($oProduct){

		try{
			foreach($oProduct as $Campo => $Valor)
			{
				$this->$Campo = $Valor;
			}

 			update_post_meta( $this->IdProducto, '_regular_price', $this->postmeta->_price );

			return "ok";
		}
		catch (Exception $e) {			
			return "Excepción capturada: ".$e->getMessage()."\n";
		}		
	}

	/**
	 * Actualiza el registro en la tabla POST con la informacion enviada por Celeste.
	 */
	protected function UpdatePost($oProduct){		
		$jsonPost = $this->GetPostById();
		if($jsonPost == null){
			return "Error al obtener el Producto desde la BD!";
		}	   
		try
		{
			$jsonPost->post_title = $oProduct->nombre;		
			$jsonPost->post_name = $oProduct->marca;
		    $jsonPost->post_content = $oProduct->descripcion;			
			$jsonPost->post_modified_gmt = date("Y-m-d h:i:s");
			$jsonPost->post_modified = date("Y-m-d h:i:s");
			$jsonPost->post_date_gmt = date("Y-m-d h:i:s");
		  

		   $this->posts = $jsonPost;
		   $this->posts->post_id = $this->IdProducto;
		   $this->posts->Guardar();	
		   return "ok";
		}
		catch (Exception $e) {			
			return  "Excepción capturada: ".$e->getMessage()."\n";
	   }
   }

	/**
	 * Busca en BD el registro asociado a la variable global  $IdProducto. Si el producto no existe
	 * devuelve null  y en caso contrario retorna el objeto producto con todas las propiedades.
	 */
	protected function GetPostById()
	{
		if($this->IdProducto == null || $this->IdProducto == "0" || $this->IdProducto == "" ){
			 return null;
		}
		
		 $this->posts->Consulta("*"," post_type='product' and id = ".$this->IdProducto." ") ;		

		if($this->posts == null){
			return null;
		}
		
		return $this->posts;
	}

	/**
	 * Obtiene un unico producto desde la Base de datos filtrado por el SKU enviado en la URL de la solicitud
	 */
	public function GetProductBySKU()
	{
		$prepared_post  = new stdClass();
		$data = array();

		if($this->sku == null || $this->sku == "" ){
			$prepared_post->status = "error";
			$prepared_post->mensaje = "Error al leer el SKU enviado como parámetro en la solicitud. Proceso de lectura cancelado.";
			return $prepared_post;
		}

		$this->postmeta->Consulta("post_id"," meta_key='_sku' and meta_value = '".$this->sku."' ");
		if($this->postmeta == null){
			$prepared_post->status = "error";
			$prepared_post->mensaje = "Error al obtener el producto desde la Base de Datos. Proceso de lectura cancelado.";
			return $prepared_post;
		}

		$this->posts->Consulta("*"," post_type='product' and id = ".$this->postmeta->post_id." ") ;

		if($this->posts == null){
			$prepared_post->status = "error";
			$prepared_post->mensaje = "Error al obtener el producto desde la Base de Datos. Proceso de lectura cancelado.";
			return $prepared_post;
		}
		
		$data['id'] = $this->posts->ID;
		$data['productStatus'] = $this->posts->post_status;					
		$data['type'] = $this->posts->post_type;		
		$data['title'] = $this->posts->post_title;	
		$data['name'] = $this->posts->post_name;	
		$data['codigo'] = get_post_meta( $this->postmeta->post_id, '_sku', true );
		$data['content'] = $this->posts->post_content;			
		$data['stockStatus']  =  get_post_meta( $this->postmeta->post_id, '_stock_status', true );
		$data['stock'] = get_post_meta( $this->postmeta->post_id, '_stock', true );
		$data['regularprice'] = get_post_meta( $this->postmeta->post_id, '_regular_price', true );
		$data['modified'] = $this->dateFormat( $this->posts->post_modified_gmt, $this->posts->post_modified );			
		
		$prepared_post->status = "ok";
		$prepared_post->mensaje = "Proceso realizado satisfactoriamente.";
		$prepared_post->data =  $data;
		return $prepared_post;
	}

	
	/**
	 * Coordina la creación de los productos enviados a través del servicio Rest.
	 * Invoca las funciones de creación de productos y metadatos por cada iteracción del
	 * ciclo foreach.
	 */
	public function BulkInsertProducts($products){
		$errores = array();
		$prepared_post  = new stdClass();
		$count=0;
		
		$x = $this->ProcesarNuevoProductoBulk($products);

// 		foreach($products as $product){
// 			
// 			$result = $this->ProcesarNuevoProducto($product);
// 			if($result->status != 'ok'){				
// 				$errores[$product->codigo] = "Error al crear el producto [".$product->codigo."] en la BD. Error -> " .$result->mensaje;				
// 				continue;
// 			}
// 			$count++;
// 		}

		if($errores){
			$prepared_post->status = "error";
			$prepared_post->mensaje ="El proceso de creación terminó con errores. Número de productos creados [".$count."]. Los siguientes productos no fueron creados ->.";
			$prepared_post->data = $errores;
			return $prepared_post;
		}

		$prepared_post->status = "ok";
		$prepared_post->mensaje = $x;
		return $prepared_post;
	}

	/**
	 * Coordina la actualización de los productos enviados a través del servicio Rest.
	 * Invoca las funciones de actualizaci+on de productos y metadatos por cada iteracción del
	 * ciclo foreach.
	 */
	public function BulkUpdateProducts($products){
		$errores = array();
		$prepared_post  = new stdClass();
		$count=0;
		foreach($products as $product){			
			$result = $this->ProcesarEdicionProducto($product,"bulk");
			if($result->status != 'ok'){				
				$errores[$product->codigo] = "Error al actualizar el producto [".$product->codigo."] en la BD. Error -> " .$result->mensaje;
				continue;
			}
			$count++;
		}

		if($errores){
			$prepared_post->status = "error";
			$prepared_post->mensaje ="El proceso de actualización terminó con errores. Numero de productos actualizados [".$count."]. Los siguientes productos no fueron actualizados->.";
			$prepared_post->data = $errores;
			return $prepared_post;
		}

		$prepared_post->status = "ok";
		$prepared_post->mensaje ="Proceso de importación realizado satisfactoriamente!";		
		return $prepared_post;
	}


	/**
	 * Obtiene el registro postmeta filtrado por SKU. Si lo encuentra, actualiza la variable global $IdProducto
	 * con el Id del Post y retorna verdadero. En caso contrario, devuelve falso y la variable  global queda signada
	 * con el valor de Cero. 
	 */
	protected function GetPostMetaBySKU($sku=null)
	{
		$this->IdProducto = '0';
		if($sku == null)
		{
			if($this->sku == null || $this->sku == "" ){				
				return false;
		   	}
		}
		else{
			$this->sku = $sku;
		}

		$this->postmeta->Consulta("post_id"," meta_key='_sku' and meta_value = '".$this->sku."' ");
		if($this->postmeta == null){
			return false;
		}
		$this->IdProducto = $this->postmeta->post_id;
		return true;
	}

	/**
	 * Le da formato al campo  fecha.
	 */
	protected function dateFormat( $date_gmt, $date = null ) {
		// Use the date if passed.
		if ( isset( $date ) ) {
			return mysql_to_rfc3339( $date );
		}

		// Return null if $date_gmt is empty/zeros.
		if ( '0000-00-00 00:00:00' === $date_gmt ) {
			return null;
		}

		// Return the formatted datetime.
		return mysql_to_rfc3339( $date_gmt );
	}



}



?>

