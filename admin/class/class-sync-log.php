<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/wp-content/plugins/sync-inventario/includes/trait-db.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/wp-content/plugins/sync-inventario/admin/class/class-sync-log-items.php';

class ClaseSyncLog
{
	use SyncDb;
	
	public function __construct()
	{
		$this->initDB("sync_log");
	}

	/**
	 * Método encargado de crear el registro principal del log con el evento realizado
	 * En el se toma del servidor la IP $_SERVER["REMOTE_ADDR"] del cliente que realiza el proceso
	 * La fecha y hora del proceso realizado.
	 * @param $Metodo Variable la cual recibe el proceso o acccion a guardar
	 */

	public function CreaLog($Metodo)
	{
		$this->sync_log_id = "0";
		$this->sync_log_ip = $_SERVER["REMOTE_ADDR"];
		$this->sync_log_fecha  = date("Y-m-d H:i:s");
		$this->sync_log_metodo  = $Metodo;
		$this->Guardar();
	}

	/**
	 * Método encargado de registrar por log los campos afectados en el procesos
	 * En el se toma la variable $Objetoy se realiza un recorrido de sus propiedades y valores.
	 * los cuales son guardados en la tabla sync_log_items
	 * Si el segundo parametro que recibe es falso, significa que no se esta creando un nuevo dato, si no que se actualiza
	 * para ello tomamos del objeto $ViejoObjeto los valores anteriores a los guardados en el proceso
	 * @param $Objeto objeto Variable que recibe un objeto con las propiedades y valores a guardar en el log	
	 * @param $Nuevo si es true solo guardara los nuevos valores, en caso de ser falso guardara los nuevos y viejos datos
	 * @param $ViejoObjeto Variable que recive un objeto con los datos anteriores a ser modificado en el proceso 
	 */

	public function RegistraCambios($Objeto,$Nuevo = true,$ViejoObjeto=null)
	{
		$objSyncItems = new ClaseSyncItems();
		$objSyncItems->sync_items_prodId = $Objeto->posts->ID;
		$objSyncItems->sync_log_id = $this->sync_log_id;

		//Creo un objeto a partir de los metas del producto
		$ViejoObjeto[1] = json_decode("{".$ViejoObjeto[1][0]->metas."}");
 		
		foreach($Objeto->getDatos() as $Campo => $Valor)
		{
			if( !is_object($Objeto->$Campo) )
			{
				$Nombre = substr($Campo,1,strlen($Campo));
				list($Tabla, $NombreCampo) = explode(":",$Objeto->$Campo);

				$VAlorAntiguo = "";
				if( !$Nuevo )
				{
					if( $Tabla == "posts" )
					{
						$VAlorAntiguo = $ViejoObjeto[0]->$NombreCampo;
					}
					else
					{
						$VAlorAntiguo = $ViejoObjeto[1]->$NombreCampo;
					}
				}

				if( $Objeto->$Nombre != "" )
				{
					$objSyncItems->sync_items_id = "0";
					$objSyncItems->sync_items_nombre = $NombreCampo;
					$objSyncItems->sync_items_antiguo = $VAlorAntiguo;
					$objSyncItems->sync_items_nuevo = $Objeto->$Nombre;
					$objSyncItems->Guardar();
				}
				else if(isset($Objeto->getDatos()->$Campo))
				{
					$objSyncItems->sync_items_id = "0";
					$objSyncItems->sync_items_nombre = $Campo;
					$objSyncItems->sync_items_antiguo = $VAlorAntiguo;
					$objSyncItems->sync_items_nuevo = $Objeto->getDatos()->$Campo;
					$objSyncItems->Guardar();
				}
			}
		}
		
	}
}



?>
