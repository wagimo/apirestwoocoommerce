<?php

require_once($_SERVER['DOCUMENT_ROOT']."/wp-content/plugins/sync-inventario/admin/class/class-sync.php");

trait DisplayConfiguracion
{
	public function displayPluginAdminDashboard() {

		require_once $_SERVER['DOCUMENT_ROOT'].'/wp-content/plugins/sync-inventario/admin/partials/'.$this->plugin_name.'-admin-display.php';
	}

	public function displayPluginAdminSettings(){

		 $active_tab = isset( $_GET[ 'tab' ] ) ? $_GET[ 'tab' ] : 'general';

		 if(isset($_GET['error_message'])){
		     add_action('admin_notices', array($this,'CrearMensajes'));
		     do_action( 'admin_notices', $_GET['error_message'] );
		 }

		 require_once $_SERVER['DOCUMENT_ROOT'].'/wp-content/plugins/sync-inventario/admin/partials/'.$this->plugin_name.'-admin-settings-display.php';
	}

	public function CrearMensajes($error)
	{
		 switch ($error)
		 {
		     case '1':
		         $message = __( 'There was an error adding this setting. Please try again.  If this persists, shoot us an email.', 'my-text-domain' );
		         $err_code = esc_attr( 'sync-inventario configuración' );
		         $setting_field = 'sync-inventario configuración';
		         break;
		 }

		 $type = 'error';
		 add_settings_error(
		        $setting_field,
		        $err_code,
		        $message,
		        $type
		    );
	}

	public function registerAndBuildFields() {

		$this->DibujFormGeneral();
		$this->DibujFormProductos();
		$this->DibujFormOrdenes();
	}

	public function DibujFormGeneral(){
		$objClaseSync = new ClaseSync();
		$objClaseSync->Consulta("*"," sync_id = 1 ");

		add_settings_section( 'sync_inventario_parametro_general', 'Parámetros Generales',  null, 'sync_inventario_parametro_general' );
		unset($CamposGenerales);
		$CamposGenerales['Dominio en donde se registra'] = array (
		      'type'      => 'input',
		      'subtype'   => 'text',
		      'id'    => 'syncDominio',
		      'name'      => 'syncDominio',
		      'required' => 'true',
		      'get_options_list' => '',
		      'value_type'=>'normal',
		      'wp_data' => $_SERVER["SERVER_NAME"],
		      'disabled' => 'true'
		  );

		$CamposGenerales['Clave secreta'] = array (
		      'type'      => 'input',
		      'subtype'   => 'text',
		      'id'    => 'syncClave',
		      'name'      => 'syncClave',
		      'required' => 'true',
		      'get_options_list' => '',
		      'value_type'=>'normal',
		      'wp_data' => $objClaseSync->sync_clave

		  );

		$CamposGenerales['Clave pública'] = array (
		      'type'      => 'input',
		      'subtype'   => 'text',
		      'id'    => 'syncPublica',
		      'name'      => 'syncPublica',
		      'required' => 'true',
		      'get_options_list' => '',
		      'value_type'=>'normal',
		      'wp_data' => $objClaseSync->sync_clave_encriptada,
		      'disabled' => 'true'

		  );

		foreach( $CamposGenerales as $Titulo => $Campos )
		{
			add_settings_field($Campos["name"],$Titulo,array( $this, 'DibujaCampos' ),'sync_inventario_parametro_general','sync_inventario_parametro_general',$Campos);
			register_setting('sync_inventario_parametro_general',$Campos["name"]);
		}
	}

	public function DibujFormProductos()
	{
		add_settings_section( 'sync_inventario_parametro_producto', 'Campos de los productos',  null, 'sync_inventario_parametro_producto' ); //"Seleccione los campos de los productos a sincronizar"
		unset($CamposGenerales);

		$objSync = new ClaseSync();
		$objSync->Consulta("*"," sync_id = 1 ");
		$objProductos = json_decode($objSync->sync_productos);


		$objSyncDb = new ClaseSyncDb();
//$objSyncDb->initDB("posts");
		$Tablas["posts"] =  array(
		   (Object)["Field" => "ID"],
		   (Object)["Field" => "post_title"],
		   (Object)["Field" => "post_name"],
		   (Object)["Field" => "post_content"],
		);
		$Tablas["postmeta"] =  array(
		   (Object)["Field" => "_sku"],
		   (Object)["Field" => "_regular_price"],
		   (Object)["Field" => "_sale_price"],
/*		   (Object)["Field" => "_sale_price_dates_from"],
		   (Object)["Field" => "_sale_price_dates_to"],
		   (Object)["Field" => "_tax_status"],
		   (Object)["Field" => "_tax_class"],
		   (Object)["Field" => "_manage_stock"],
		   (Object)["Field" => "_backorders"],
		   (Object)["Field" => "_sold_individually"],
		   (Object)["Field" => "_weight"],
		   (Object)["Field" => "_length"],
		   (Object)["Field" => "_width"],
		   (Object)["Field" => "_height"],
		   (Object)["Field" => "_upsell_ids"],
		   (Object)["Field" => "_crosssell_ids"],
		   (Object)["Field" => "_purchase_note"],
		   (Object)["Field" => "_default_attributes"],*/
		   (Object)["Field" => "_stock"],
// 		   (Object)["Field" => "_stock_status"],
// 		   (Object)["Field" => "_product_version"],
		   (Object)["Field" => "_price"],
// 		   (Object)["Field" => "total_sales"] 
		);

		foreach( $Tablas as $Tabla => $Campos)
		{
			foreach( $Campos as $Campo )
			{
				$Checked = false;
				$Valor = "";
				if( isset($objProductos->$Tabla) && is_object($objProductos->$Tabla) )
				{
					$Nombre = $Campo->Field;
					if( isset($objProductos->$Tabla->$Nombre) )
					{
						$Checked = true;
						$Valor = $objProductos->$Tabla->$Nombre;
					}
				}

				$CamposGenerales[$Campo->Field] = array (
				'type'      => 'input',
				'subtype'   => 'checkbox',
				'id'    => $Campo->Field,
				'name'      => $Campo->Field,
				'required' => 'true',
				'get_options_list' => '',
				'value_type'=>'normal',
				'wp_data' => $Valor,
				'disabled' => 'false',
				'checked' => $Checked,
				'ValorCheck' => $Tabla.":".$Campo->Field
				);
			}
		}
		foreach( $CamposGenerales as $Titulo => $Campos )
		{
			add_settings_field($Campos["name"],$Titulo,array( $this, 'DibujaCampos' ),'sync_inventario_parametro_producto','sync_inventario_parametro_producto',$Campos);
			register_setting('sync_inventario_parametro_producto',$Campos["name"]);
		}
	}

	public function DibujFormOrdenes()
	{
		add_settings_section( 'sync_inventario_parametro_ordenes', 'Campos de las ordenes de compra',  null, 'sync_inventario_parametro_ordenes' ); //"Seleccione los campos de las ordenes a sincronizar"
		unset($CamposGenerales);

		$objSync = new ClaseSync();
		$objSync->Consulta("*"," sync_id = 1 ");
		$objOrdenes = json_decode($objSync->sync_ordenes);


		$objSyncDb = new ClaseSyncDb();

		$Tablas["posts"] =  $objSyncDb->initDB("posts");
		$Tablas["postmeta"] =  array(
		   (Object)["Field" => "_sku"],
		   (Object)["Field" => "_regular_price"],
		   (Object)["Field" => "_sale_price"],
		   (Object)["Field" => "_sale_price_dates_from"],
		   (Object)["Field" => "_sale_price_dates_to"],
		   (Object)["Field" => "_tax_status"],
		   (Object)["Field" => "_tax_class"],
		   (Object)["Field" => "_manage_stock"],
		   (Object)["Field" => "_backorders"],
		   (Object)["Field" => "_sold_individually"],
		   (Object)["Field" => "_weight"],
		   (Object)["Field" => "_length"],
		   (Object)["Field" => "_width"],
		   (Object)["Field" => "_height"],
		   (Object)["Field" => "_upsell_ids"],
		   (Object)["Field" => "_crosssell_ids"],
		   (Object)["Field" => "_purchase_note"],
		   (Object)["Field" => "_default_attributes"],
		   (Object)["Field" => "_stock"],
		   (Object)["Field" => "_stock_status"],
		   (Object)["Field" => "_product_version"],
		   (Object)["Field" => "_price"],
		   (Object)["Field" => "total_sales"] );

		foreach( $Tablas as $Tabla => $Campos)
		{
			foreach( $Campos as $Campo )
			{
				$Checked = false;
				$Valor = "";
				if( isset($objOrdenes->$Tabla) && is_object($objOrdenes->$Tabla) )
				{
					$Nombre = $Campo->Field;
					if( isset($objOrdenes->$Tabla->$Nombre) )
					{
						$Checked = true;
						$Valor = $objOrdenes->$Tabla->$Nombre;
					}
				}

				$CamposGenerales[$Campo->Field] = array (
				'type'      => 'input',
				'subtype'   => 'checkbox',
				'id'    => $Campo->Field,
				'name'      => $Campo->Field,
				'required' => 'true',
				'get_options_list' => '',
				'value_type'=>'normal',
				'wp_data' => $Valor,
				'disabled' => 'false',
				'checked' => $Checked,
				'ValorCheck' => $Tabla.":".$Campo->Field
				);
			}
		}
		foreach( $CamposGenerales as $Titulo => $Campos )
		{
			add_settings_field($Campos["name"],$Titulo,array( $this, 'DibujaCampos' ),'sync_inventario_parametro_ordenes','sync_inventario_parametro_ordenes',$Campos);
			register_setting('sync_inventario_parametro_ordenes',$Campos["name"]);
		}
	}

	public function DibujaCampos($args) {

		if($args['wp_data'] == 'option'){
			$wp_data_value = get_option($args['name']);
		}
		elseif($args['wp_data'] == 'post_meta'){
			$wp_data_value = get_post_meta($args['post_id'], $args['name'], true );
		}
		else{
			$wp_data_value = $args['wp_data'];
		}

		switch ($args['type']) {

			case 'input':
			    $value = ($args['value_type'] == 'serialized') ? serialize($wp_data_value) : $wp_data_value;
			    if($args['subtype'] != 'checkbox'){
				$prependStart = (isset($args['prepend_value'])) ? '<div class="input-prepend"> <span class="add-on">'.$args['prepend_value'].'</span>' : '';
				$prependEnd = (isset($args['prepend_value'])) ? '</div>' : '';
				$step = (isset($args['step'])) ? 'step="'.$args['step'].'"' : '';
				$min = (isset($args['min'])) ? 'min="'.$args['min'].'"' : '';
				$max = (isset($args['max'])) ? 'max="'.$args['max'].'"' : '';
				if(isset($args['disabled'])){
				    echo $prependStart.'<input type="'.$args['subtype'].'" id="'.$args['id'].'_disabled" '.$step.' '.$max.' '.$min.' name="'.$args['name'].'_disabled" size="40" disabled value="' . esc_attr($value) . '" /><input type="hidden" id="'.$args['id'].'" '.$step.' '.$max.' '.$min.' name="'.$args['name'].'" size="40" value="' . esc_attr($value) . '" />'.$prependEnd;
				} else {
				    echo $prependStart.'<input type="'.$args['subtype'].'" id="'.$args['id'].'" "'.$args['required'].'" '.$step.' '.$max.' '.$min.' name="'.$args['name'].'" size="40" value="' . esc_attr($value) . '" />'.$prependEnd;
				}

			    } else {
				$checked = $args['checked'] ? 'checked' : '';
				echo '<input type="'.$args['subtype'].'" id="chk'.$args['id'].'" "'.$args['required'].'" name="chk'.$args['name'].'" size="40" value="'.$args['ValorCheck'].'" '.$checked.' />';
				echo '<input type="text" id="'.$args['id'].'" "'.$args['required'].'" '.$step.' '.$max.' '.$min.' name="'.$args['name'].'" size="40" value="' . esc_attr($value) . '" />';
			    }
			    break;
			default:
			    # code...
			    break;
		}
	}
}



?>
