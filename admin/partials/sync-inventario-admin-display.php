<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    sync-inventario
 * @subpackage sync-inventario/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<div class="wrap">
	<div id="icon-themes" class="icon32"></div>
	<h2>Sync Inventario Settings</h2>
	<!--NEED THE settings_errors below so that the errors/success messages are shown after submission - wasn't working once we started using add_menu_page and stopped using add_options_page so needed this-->
	<?php settings_errors(); ?>


	<nav class="nav-tab-wrapper woo-nav-tab-wrapper pestanas">
		<input type="radio" id="rdbGeneral" name="rdbOpcion" onmousedown="rdbForm1.checked=true" checked="true" value="1" />
		<label class="nav-tab General" for="rdbGeneral" onmousedown="rdbForm1.checked=true">General</label>
		<input type="radio" id="rdbProducto" name="rdbOpcion" onmousedown="rdbForm2.checked=true" value="1" />
		<label class="nav-tab Productos" for="rdbProducto" onmousedown="rdbForm2.checked=true">Productos</label>
<!--		<label class="nav-tab ">Orden de compra</label>-->
	</nav>

	<input type="radio" class="rdbForm" id="rdbForm1" name="rdbForm" value="1" checked="true" />
	<div class="FormPestana">
		<form method="POST" action="/wp-content/plugins/sync-inventario/admin/callbacks-configuracion.php">
		    <?php
			settings_fields( 'sync_inventario_parametro_general' );
			do_settings_sections( 'sync_inventario_parametro_general' );
		    ?>
		    <?php submit_button(); ?>
		</form>
	</div>

	<input type="radio" class="rdbForm" id="rdbForm2" name="rdbForm" value="2"  />
	<div class="FormPestana">
		<form method="POST" action="/wp-content/plugins/sync-inventario/admin/callbacks-productos.php">
		    <?php
			settings_fields( 'sync_inventario_parametro_producto' );
			do_settings_sections( 'sync_inventario_parametro_producto' );
		    ?>
		    <?php submit_button(); ?>
		</form>
	</div>
<!--	<div >
		<form method="POST" action="/wp-content/plugins/sync-inventario/admin/callbacks-ordenes.php">
		    <?php
		//	settings_fields( 'sync_inventario_parametro_ordenes' );
		//	do_settings_sections( 'sync_inventario_parametro_ordenes' );
		    ?>
		    <?php // submit_button(); ?>
		</form>
	</div>-->
</div>
