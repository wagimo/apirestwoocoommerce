<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/wp-content/plugins/sync-inventario/admin/class/class-productos.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/wp-content/plugins/sync-inventario/admin/class/class-ordenes.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/wp-content/plugins/sync-inventario/admin/class/class-sync.php';

class WP_REST_Async_Controller extends WP_REST_Controller {



    public function __construct() {
		$this->namespace = 'async-Api/v1';
		$this->rest_base = 'async';       
		$this->post_type = 'product';
	}

    public function register_routes() {
        
        /**
         * EndPoint para retornar las ordenes de compra en estado Completed y para crear un nuevo producto y sus 
         * metadatos en la base de datos.
         */
		register_rest_route( 
            $this->namespace, 
            '/' . $this->rest_base, 
            array(
                array(
                    'methods'             => WP_REST_Server::READABLE,
                    'callback'            => array( $this, 'get_orders' ),
                    'permission_callback' => array( $this, 'validation_permissions_check' ),
                    'args'                => $this->get_collection_params(),
                ),
                array(
                            'methods'             => WP_REST_Server::CREATABLE,
                            'callback'            => array( $this, 'post_product' ),
                            'permission_callback' => array( $this, 'validation_permissions_check' ),
                            'args'                => $this->get_endpoint_args_for_item_schema( WP_REST_Server::CREATABLE ),
                )
                            
		));
		
        /**
         * ErdPoint utilizado para actualizar la información del  producto (post)  y sus metadatos (postMeta).
         */
        register_rest_route( $this->namespace, '/' . $this->rest_base . '/(?P<sku>\d+)', array(
			array(
                'methods'             => WP_REST_Server::EDITABLE,
                'callback'            => array( $this, 'update_product' ),
                'permission_callback' => array( $this, 'validation_permissions_check' ),
                'args'                => $this->get_endpoint_args_for_item_schema( WP_REST_Server::EDITABLE ),
            )
		) );

        /**
         * EndPoint utilizado para actualizar las cantidades en el inventario de productos.
         */
        register_rest_route( $this->namespace, '/' .$this->rest_base. '/inventario/(?P<sku>\d+)', array(
			array(
                'methods'             => WP_REST_Server::EDITABLE,
                'callback'            => array( $this, 'update_inventario' ),
                'permission_callback' => array( $this, 'validation_permissions_check' ),
                'args'                => $this->get_endpoint_args_for_item_schema( WP_REST_Server::EDITABLE ),
            )
		) );

        /**
         * EndPoint utilizado para crear o actualizar masivamente productos en la base de datos.
         * 1-creacion
         * 2-actualizacion
        */
        register_rest_route( $this->namespace, '/products/async/post/(?P<type>[a-zA-Z0-9-]+)', array(
			array(
                'methods'             => WP_REST_Server::CREATABLE,
                'callback'            => array( $this, 'post_product_all' ),
                'permission_callback' => array( $this, 'validation_permissions_check' ),
                'args'                => $this->get_endpoint_args_for_item_schema( WP_REST_Server::CREATABLE ),
            )    
		) );

        /**
         * EndPoint que retorna un Producto y sus metadatos desde las base de datos. (En construcción)
         */
		register_rest_route( $this->namespace, '/' . $this->rest_base . '/(?P<sku>[\d]+)', array(
			array(
				'methods'             => WP_REST_Server::READABLE,
				'callback'            => array( $this, 'get_productBySku' ),
				'permission_callback' => array( $this, 'validation_permissions_check' ),
				'args'                => array(
					'context' => $this->get_context_param( array(
						'default' => 'view',
					) ),
				),
			)
		) );
	}

    /**
     * Obtiene las ordenes de compra que se encuentran en el  estado completed 
     * dentro del E-commerce, para que celeste genere las facturas respectivas.
     */
    public function get_orders($request){

// 	//return print_r( $this->get_headers( wp_unslash( $_SERVER ) ),true);
// 	//return  $this->get_headers( wp_unslash( $_SERVER ) );
// 	return $request->get_header('authorization');

        $param = $request->get_param( 'status' );
        if($param == null){
          $param = 'completed';
        }
       //exit("Registrando Rutas");
        $objOrden = new ClaseOrdenes();
        $response = $objOrden->GetOrderTodos($param);

       $res = new WP_REST_Response($response);
       $res->set_status(200);

       return ['req' => $res];
    }

    /**
     * Permite la creación de un producto haciendo uso de las clases
     * que consumen los servicios nativos de wordpress y woocommerce
     */
    public function post_product($request){
        
        
        $invalid_params  = array();
        $invalid_details = array();

       $body = $request->get_body(); 
       if( $body == null )      {
            $invalid_params["body"] =  "BODY";
            $invalid_details["body"] = "Error al leer el JSON desde el body de la solicitud. Json no encontrado!! " ;
       }
      

        $productObject = json_decode($body);
	
	foreach( $productObject as $Campo => $Valor )
	{
		if( is_numeric($Valor) && $Valor <= 0 )
		{
			$productObject->$Campo = 0;
		}
	}
        
        if($productObject == null){
            $invalid_params["product"] =  "PRODUCT";
            $invalid_details["product"] = "Error al serializar el objeto JSON. Proceso de actualización cancelado!" ;
        }
		
        if($invalid_params){
            return new WP_Error(
                'rest_invalid_param',                 
                 sprintf( __( 'Invalid parameter(s): %s' ), implode( ', ', array_keys( $invalid_params ) ) ),
                 array(
                         'status'  => 400,
                         'params'  => $invalid_params,
                         'details' => $invalid_details,
                    )
                );
        }

         $objProducto = new ClaseProductos();	    
         $post = $objProducto->ProcesarNuevoProducto($productObject);
         $res = new WP_REST_Response( $post );     

         if($post->status == "ok"){                     
            $res->set_status(200);
         }else{ 
            $res->set_status(500);
         }                        
         return $res;
    }
   
    /**
    * Permite la Actualizacion de un producto haciendo uso de las clases
     * que consumen los servicios nativos de wordpress y woocommerce
     */
    public function update_product($request){
       
        $param = $request->get_param( 'sku' );
        $invalid_params  = array();
        $invalid_details = array();

        if($param <= 0){
            $invalid_params["sku"] =  "SKU";
            $invalid_details["sku"] = "El SKU debe ser un número mayor a cero!" ;
        }  

       $body = $request->get_body(); 
       if( $body == null )      {
            $invalid_params["body"] =  "BODY";
            $invalid_details["body"] = "Error al leer el JSON desde el body de la solicitud. Json no encontrado!! " ;
       }
      

        $productObject = json_decode($body);  
        
        if($productObject == null){
            $invalid_params["product"] =  "PRODUCT";
            $invalid_details["product"] = "Error al serializar el objeto JSON. Proceso de actualización cancelado!" ;
        }
		
        if($invalid_params){
            return new WP_Error(
                'rest_invalid_param',                 
                 sprintf( __( 'Invalid parameter(s): %s' ), implode( ', ', array_keys( $invalid_params ) ) ),
                 array(
                         'status'  => 400,
                         'params'  => $invalid_params,
                         'details' => $invalid_details,
                    )
                );
        }

         $objProducto = new ClaseProductos();
	     $objProducto->sku = $param;
        
        
         $post = $objProducto->ProcesarEdicionProducto($productObject);
        
         $res = new WP_REST_Response( $post );       

         if($post->status == "ok"){           
            $res->set_status(200);
         }else{            
            $res->set_status(500);
         }    
                   
         return $res;
    }

    /**
     * Permite la Actualización del inventario por cada operación realizada en celeste.
     * Las 4 operaciones permitidas son: Venta, Compra, Devolucion, NotaInventario.
     */
    public function update_inventario($request){
        $param = $request->get_param( 'sku' );
        $invalid_params  = array();
        $invalid_details = array();

        if($param <= 0){
            $invalid_params["sku"] =  "SKU";
            $invalid_details["sku"] = "El SKU debe ser un número mayor a cero!" ;
        }  

       $body = $request->get_body(); 
       if( $body == null )      {
            $invalid_params["body"] =  "BODY";
            $invalid_details["body"] = "Error al leer el JSON desde el body de la solicitud. Json no encontrado!! " ;
       }
      

        $productObject = json_decode($body);  
        
        if($productObject == null){
            $invalid_params["product"] =  "PRODUCT";
            $invalid_details["product"] = "Error al serializar el objeto JSON. Proceso de actualización cancelado!" ;
        }
		
        if($invalid_params){
            return new WP_Error(
                'rest_invalid_param',                 
                 sprintf( __( 'Invalid parameter(s): %s' ), implode( ', ', array_keys( $invalid_params ) ) ),
                 array(
                         'status'  => 400,
                         'params'  => $invalid_params,
                         'details' => $invalid_details,
                    )
                );
        }

         $objProducto = new ClaseProductos();
	     $objProducto->sku = $param;
        
        
         $post = $objProducto->AjustarCantidadesInventario($productObject);
        
         $res = new WP_REST_Response( $post );       

         if($post->status == "ok"){           
            $res->set_status(200);
         }else{            
            $res->set_status(500);
         }    
                   
         return $res;
    }


    /**
     * Obtiene un unico producto desde la base de datos
     */
    public function get_productBySku($request){

        $param = $request->get_param( 'sku' );
        $invalid_params  = array();
        $invalid_details = array();

        if($param <= 0){
            $invalid_params["sku"] =  "SKU";
            $invalid_details["sku"] = "El SKU debe ser un número mayor a cero!" ;
        }  		
        if($invalid_params){
            return new WP_Error(
                'rest_invalid_param',                 
                 sprintf( __( 'Invalid parameter(s): %s' ), implode( ', ', array_keys( $invalid_params ) ) ),
                 array(
                         'status'  => 400,
                         'params'  => $invalid_params,
                         'details' => $invalid_details,
                    )
                );
        }

         $objProducto = new ClaseProductos();
	     $objProducto->sku = $param;
        
        
         $post = $objProducto->GetProductBySKU();
        
         $res = new WP_REST_Response( $post );       

         if($post->status == "ok"){           
            $res->set_status(200);
         }else{            
            $res->set_status(500);
         }    
                   
         return $res;
    }

    /**
     * Recibe un conjunto de registros para ser creados de forma masiva en la BD.
     */
    public function post_product_all($request){

        $prepared_post  = new stdClass();
        $invalid_params  = array();
        $invalid_details = array();

        $param = $request->get_param( 'type' );
       
        if($param != 'create' && $param != 'update'){
             $invalid_params["type"] =  "QueryString";
             $invalid_details["type"] = "El valor enviado en la cadena de consulta no coincide con las dos operaciones habilitadas para el proceso de sincronizacion masiva: [create]  o [update]. Proceso cancelado." ;
        }      
       

        $body = json_decode( $request->get_body() ); 
        if( $body == null )      {
             $invalid_params["body"] =  "BODY";
             $invalid_details["body"] = "Error al leer el JSON desde el body de la solicitud. Json no encontrado!! " ;
        }

       

        if($invalid_params){
            return new WP_Error(
                'rest_invalid_param',                 
                 sprintf( __( 'Invalid parameter(s): %s' ), implode( ', ', array_keys( $invalid_params ) ) ),
                 array(
                         'status'  => 500,
                         'params'  => $invalid_params,
                         'details' => $invalid_details,
                    )
                );
        }

       
         $objProducto = new ClaseProductos();	
        // $post->status = "ok";
        // $post->mensaje = $param;
       
        switch($param){
            case "create":                 
                $post = $objProducto->BulkInsertProducts($body);
                break;
            case "update":
                $post = $objProducto->BulkUpdateProducts($body);
                break;
           
        }
        
        $res = new WP_REST_Response( $post );     

        if($post->status == "ok"){                     
           $res->set_status(200);
        }else{ 
           $res->set_status(500);
        }                        
        return $res;
    }

    public function prueba( $request){
        $prepared_post  = new stdClass();
       
       
         $prueba = array();

         $body = json_decode( $request->get_body() ); 
       
        
        foreach($body as $b){
            $prueba[$b->sku] = $b->nombre;
        }

         $res = new WP_REST_Response($prueba);
         $res->set_status(200);

         return $res;
    }
    
    /**
     * Determina si la solicitud hecha al endpoint tiene los permisos adecuados para
     * el acceso a la data. Validación del token de autenticación enviado en los headers
     * de la solicitud.
     */
    public function validation_permissions_check($request){
	$objSync = new ClaseSync();
	$Llave = $request->get_header('authorization');
        return $objSync->ValidaLlave($Llave);
    }


	public function get_headers( $server ) {
		$headers = array();
		
		// CONTENT_* headers are not prefixed with HTTP_.
		$additional = array( 'CONTENT_LENGTH' => true, 'CONTENT_MD5' => true, 'CONTENT_TYPE' => true );
		
		foreach ( $server as $key => $value ) {
			if ( strpos( $key, 'HTTP_' ) === 0 ) {
			$headers[ substr( $key, 5 ) ] = $value;
			} elseif ( isset( $additional[ $key ] ) ) {
			$headers[ $key ] = $value;
			}
		}
		
		return $headers;
	}

}

