<?php

/**
 * Fired during plugin activation
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Plugin_Name
 * @subpackage Plugin_Name/includes
 * @author     Your Name <email@example.com>
 */
 
 require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
 
class sync_inventario_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		self::AdicionaPermisos();
		self::CreaTablas();
	}
	
	public static function CreaTablas(){
	
		global $wpdb;

		$Tabla["sync_log"] = "CREATE TABLE `".$wpdb->prefix."sync_log` (
					  `sync_log_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id',
					  `sync_log_ip` varchar(20) NOT NULL COMMENT 'Ip desde donde se sincroniza',
					  `sync_log_fecha` datetime NOT NULL COMMENT 'Fecha de la sincronizan',
					  `sync_log_metodo` varchar(25) NOT NULL COMMENT 'Metodo usado',
					   PRIMARY KEY (`sync_log_id`)
					) ENGINE=InnoDB DEFAULT CHARSET=latin1;
					";

		$Tabla["sync_log_items"]  = "CREATE TABLE `".$wpdb->prefix."sync_log_items` (
						  `sync_items_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id',
						  `sync_items_prodId` int(11) NOT NULL COMMENT 'Id del producto',
						  `sync_items_nombre` varchar(25) NOT NULL COMMENT 'Nombre del producto',
						  `sync_items_nuevo` blob DEFAULT NULL COMMENT 'Valor antiguo del campo',
						  `sync_items_antiguo` blob DEFAULT NULL COMMENT 'Valor antiguo del campo',
						  `sync_log_id` int(11) NOT NULL COMMENT 'Id de la tabla de log',
						  PRIMARY KEY (`sync_items_id`),
						  KEY `sync_log_id` (`sync_log_id`)
						) ENGINE=InnoDB DEFAULT CHARSET=latin1;
						  ";

		$Tabla["sync"]  = "CREATE TABLE `".$wpdb->prefix."sync` (
						  `sync_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
						  `sync_dominio` varchar(25) NOT NULL COMMENT 'Dominio del sitio',
						  `sync_clave` varchar(25) NOT NULL,
						  `sync_clave_encriptada` varchar(100) NOT NULL,
						  `sync_productos` BLOB NOT NULL COMMENT 'Json de productos',
						  `sync_ordenes` BLOB NOT NULL COMMENT 'Json de ordenes',
						  PRIMARY KEY (`sync_id`)
						) ENGINE=InnoDB DEFAULT CHARSET=latin1;
						";
		foreach( $Tabla as $Nombre => $Sql )
		{
			if( !maybe_create_table( $wpdb->prefix.$Nombre, $Sql ) )
			{
				echo "Error en la consulta:".$Sql;
				exit(0);
			}
		}
	}
	
	// Add the new capability to all roles having a certain built-in capability
	private static function AdicionaPermisos(){
	
	        $roles = get_editable_roles();
        	foreach ($GLOBALS['wp_roles']->role_objects as $key => $role)
        	{
            		if (isset($roles[$key]) )
            		{
            			//var_dump($roles);exit(0);
                		$role->add_cap('sync_settings_page',true);
//                 		$role->add_cap('sync_about_page',true);

                		$role->add_cap('sync-inventario',true);

            		}
        	}
	}
}


