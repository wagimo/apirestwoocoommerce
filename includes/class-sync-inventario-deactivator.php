<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Plugin_Name
 * @subpackage Plugin_Name/includes
 * @author     Your Name <email@example.com>
 */
class sync_inventario_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {
		self::RemuevePermisos();
		self::BorrarTablas();
	}
	
	private static function RemuevePermisos() {
	        $roles = get_editable_roles();
        	foreach ($GLOBALS['wp_roles']->role_objects as $key => $role) 
        	{
	            if (isset($roles[$key]) && $role->has_cap('sync_settings_page')) 
	            {
		        $role->remove_cap('sync_settings_page');
            	    }

	            if (isset($roles[$key]) && $role->has_cap('sync_about_page')) 
	            {
		        $role->remove_cap('sync_about_page');
            	    }
		
		}
	}	
	
	public static function BorrarTablas(){
	
		$Tabla = array( 0 => "sync_log", 1 => "sync_log_items", 2 => "sync" );
	
		global $wpdb;
		foreach( $Tabla as $Nombre )
		{
			$sql = "DROP TABLE IF EXISTS ".$wpdb->prefix.$Nombre;
			$wpdb->query($sql);
		}
	}

}
