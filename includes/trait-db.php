<?php

trait SyncDb {

	private $Tabla;
	private $PK = "";

	function initDB($_Tabla = "")
	{
		$this->Tabla = $_Tabla;
		
		$Columnas = $this->ObtieneCampos();
		
		foreach( $Columnas as $Columna )
		{
 			$Campo = $Columna->Field;
 			$this->$Campo = "";

			IF( $Columna->Key == "PRI" )
			{
				$this->PK = $Columna->Field;
			}
		}
		return $Columnas;	
	}
	
	public function ObtieneCampos()
	{
		global $wpdb;
		return $wpdb->get_results( " SHOW COLUMNS FROM ".$wpdb->prefix.$this->Tabla, OBJECT );
	}
	
	public function Guardar($Where = null)
	{
		
		
		global $wpdb;
		$PK = $this->PK;
		$Columnas = $this->ObtieneCampos();
		$Data = array();
		foreach( $Columnas as $Columna )
		{
			$Campo = $Columna->Field;
			$Data[$Campo] = $this->$Campo;
		}
		
		if( $this->$PK == "0" || $this->$PK == "" )
		{
			//$wpdb->show_errors(); 
			$wpdb->insert($wpdb->prefix.$this->Tabla,$Data);	
			//$wpdb->print_error();
			$this->$PK = $wpdb->insert_id;
		}
		else
		{
			if( $Where == null )
			{
				$Where = array($this->PK => $this->$PK);
			}
			$wpdb->show_errors(); 
			$wpdb->update($wpdb->prefix.$this->Tabla,$Data,$Where);
			$wpdb->print_error();
		}

	}

	public function Consulta($Columnas = "*",$Where="1")
	{
		global $wpdb;
		$Data = $wpdb->get_results( " SELECT $Columnas FROM ".$wpdb->prefix.$this->Tabla." where ".$Where, OBJECT );
		if( count($Data) == 1 ) 
		{

			foreach( $Data[0] as $Campo => $Valor )
			{
				if(isset($this->$Campo))
				{
					$this->$Campo = $Valor;
				}
			}
		}
		else if( count($Data) == 0 ) 
		{
			$this->$PK = "0";
		}

		return $Data;
	}

	public function ConsultaJoin($sql){
		global $wpdb;
		return $wpdb->get_results($sql);
    }

}

class ClaseSyncDb
{
	use SyncDb;
}

