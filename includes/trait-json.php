<?php

require_once($_SERVER['DOCUMENT_ROOT']."/wp-content/plugins/sync-inventario/admin/class/class-sync.php");


trait SyncJson {

	private $Datos;
	private $Inicia = false;

	function init()
	{
		$this->Datos = new stdClass();
	}

	function initJSON($Proceso)
	{
		//$Metas = array( _edit_last,_edit_lock,_sku,_regular_price,_sale_price,total_sales,_tax_status,_tax_class,_manage_stock,_backorders,_sold_individually,_virtual,_downloadable,_download_limit,_download_expiry,_stock,_stock_status,_wc_average_rating,_wc_review_count,_product_version,_price )
		$this->Datos = new stdClass();
		$objClaseSync = new ClaseSync();
		$objClaseSync->Consulta("*"," sync_id = 1 ");

		$Objeto = $Proceso == "Productos" ? json_decode($objClaseSync->sync_productos) : json_decode($objClaseSync->sync_ordenes);
		foreach( $Objeto as $Tabla => $Campos )
		{
 			$this->Datos->$Tabla = new ClaseSyncDb();
 			$this->Datos->$Tabla->initDB($Tabla);

			foreach( $Campos as $Nombre => $Valor )
			{
				$Valor = "_".$Valor;
				$this->Datos->$Valor = $Tabla.":".$Nombre;
			}
		}
	}

	public function getDatos()
	{
		return $this->Datos;
	}

 	public function __get($Nombre)
 	{
 		if( property_exists($this->Datos,"_".$Nombre) )
 		{
			$Nombre = "_".$Nombre;
 			list($Tabla,$Campo) = explode(":",$this->Datos->$Nombre);

  			if( $Tabla == "postmeta" && $this->IdProducto != "0" )
  			{
				return get_post_meta($this->IdProducto,$Campo,true);
  			}
 			return $this->Datos->$Tabla->$Campo;
 		}
 		return $this->Datos->$Nombre;
 	}

	public function __set($Nombre, $Valor)
	{
		if( property_exists($this->Datos,"_".$Nombre) )
		{
			$Nombre = "_".$Nombre;
			list($Tabla,$Campo) = explode(":",get_object_vars($this->Datos)[$Nombre]);
			$this->Datos->$Tabla->$Campo = $Valor;

  			if( $Tabla == "postmeta" && $this->IdProducto != "0" )
  			{
				update_post_meta($this->IdProducto,$Campo,$Valor);
  			}
		}
		else
		{
			$this->Datos->$Nombre = $Valor;
		}
 	}

}

class ClaseSyncJson
{
	use SyncJson;
}