<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://example.com
 * @since             1.0.0
 * @package           sync_inventario
 *
 * @wordpress-plugin
 * Plugin Name:       Sincronizacion de inventarios
 * Plugin URI:        http://nautilius.com.co
 * Description:       PlugIn que expone EndPoints para la sincronización de inventarios y ordenes de compra
 * Version:           1.0.0
 * Author:            LineVox - Nautilius
 * Author URI:        http://nautilius.com.co/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       sync-inventario
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Registra el controlador el cual contiene los puntos finales
 * a exponer en la apiRest.
 */
add_action( 'rest_api_init', function () {
	
	require plugin_dir_path( __FILE__ ).'includes/ApiRest-endpoint.php';
	$controller = new WP_REST_Async_Controller();
	$controller->register_routes();
} );

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'sync_inventario_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-sync-inventario-activator.php
 */
function activate_sync_inventario() {
	require_once plugin_dir_path( __FILE__ ).'includes/class-sync-inventario-activator.php';
	sync_inventario_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-sync-inventario-deactivator.php
 */
function deactivate_sync_inventario() {
	require_once plugin_dir_path( __FILE__ ).'includes/class-sync-inventario-deactivator.php';
	sync_inventario_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_sync_inventario' );
register_deactivation_hook( __FILE__, 'deactivate_sync_inventario' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ).'includes/class-sync-inventario.php';



/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_sync_inventario() {

	$plugin = new sync_inventario();
	$plugin->run();

}
run_sync_inventario();

